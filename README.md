# Фабрика Решений тестовое задание "Сервис уведомлений"

## Задание

[Перейти](https://www.craft.do/s/n6OVYFVUpq0o6L)

## Тестовый сервер

Сборка выложена на [выделенный сервер](http://89.22.227.82:8000/) для просмотра функционала без установки локально

## Документация

[Swagger UI](http://89.22.227.82:8000/docs/)

[OpenApi](http://89.22.227.82:8000/openapi/)


## Запуск

### С Докером

1. Клонировать проект
    ```shell 
    git clone https://gitlab.com/fenrir1121/factory_testwork
   ```

2. Создать .env файл, заполнить своими данными
    ```shell 
    cp .env_example .env
    ```
3. Запустить
    ```shell
    docker-compose up
    ```

### Без Докера

1. Клонировать проект
    ```shell 
    git clone https://gitlab.com/fenrir1121/factory_testwork
   ```
   
2. Установить *poetry*
   [(инструкция)](https://www.jetbrains.com/help/pycharm/poetry.html)
   ```shell
   curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -
   ```
   Для Windows Powershell
   ```shell
   (Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python -
   ```

3. Устанавливаем зависимости проекта
   ```shell
   poetry install
   ```
   
4. Скачиваем и запускаем *redis*
   - Для ubuntu
   ```shell
   sudo add-apt-repository ppa:redislabs/redis
   sudo apt update
   sudo apt install redis-server
   ```
   - Для [Windows](https://github.com/microsoftarchive/redis/releases/tag/win-3.2.100)
   
5. Запустить *celery*
   ```shell
   celery -A fvProject worker -l info --pool=solo
   ```
   
6.Запустить проект
   ```shell
   python manage.py runserver 127.0.0.1:8000
   ```

## Дополнительные задания

- Добавлены тесты моделей
- Подготовлен docker-compose
- Добавлена документация Swagger UI
- Добавлена откладывание запросов