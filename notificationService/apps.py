from django.apps import AppConfig


class NotificationserviceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'notificationService'

    def ready(self):
        from notificationService import signals
