from django.db import models
from django.core.validators import RegexValidator
from django.utils import timezone

import pytz
from datetime import datetime, timedelta


class Distribution(models.Model):
    """
    Сущность "рассылка"
    """

    dt_start = models.DateTimeField(verbose_name='Начало рассылки', default=timezone.now)
    message = models.TextField(verbose_name='Текст сообщения клиенту', max_length=255)
    filter_tag = models.CharField(verbose_name='Поиск по меткам', max_length=255, blank=True)
    _opr_code = RegexValidator(regex=r'^\d{3}$', message='Код в формате XXX (X - цифра)')
    filter_code = models.CharField(verbose_name='Поиск по оператору', validators=[_opr_code], max_length=3, blank=True)
    dt_end = models.DateTimeField(verbose_name='Конец рассылки', default=timezone.now)

    def sending(self, utc) -> bool:
        tz = pytz.timezone(utc)
        now = datetime.now(tz)
        return True if self.dt_start <= now <= self.dt_end else False

    def __str__(self):
        return f"Рассылка от {self.dt_start}: {self.message}"

    class Meta:
        verbose_name = "Distribution"


class Client(models.Model):
    """
    Сущность "клиент"
    """
    # https://stackoverflow.com/questions/9454212/save-time-zone-in-django-models
    _TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    _phone_re = RegexValidator(regex=r'^7\d{10}$', message='Номер в формате 7XXXXXXXXXX (X - цифра)')
    phone = models.CharField(verbose_name='Номер клиента', validators=[_phone_re], unique=True, max_length=11)

    opr_code = models.CharField(verbose_name='Код оператора', editable=False, max_length=3)
    tag = models.CharField(verbose_name='Тег клиента (может быть пустым)', max_length=100, blank=True)
    utc = models.CharField(verbose_name='Часовой пояс', max_length=32, choices=_TIMEZONES)

    def save(self, *args, **kwargs):
        self.opr_code = self.phone[1:4]
        return super(Client, self).save(*args, **kwargs)

    def __str__(self):
        return f"Клиент {self.phone} {'('+self.tag+')' if self.tag else ''}"

    class Meta:
        verbose_name = "Client"


class Message(models.Model):
    """
    Сущность "сообщение"
    """

    created_at = models.DateTimeField(verbose_name='Время создания(отправки)', auto_now_add=True)
    distribution = models.ForeignKey(Distribution, verbose_name='Рассылка', on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, verbose_name='Клиент', on_delete=models.CASCADE, related_name='messages')
    sending = models.BooleanField(verbose_name='Отправлено', default=False, editable=False)

    def __str__(self):
        return f"Сообщение(id={self.id}, client={self.client.id}, created_at={self.created_at}, sending={self.sending})"

    class Meta:
        verbose_name = "Message"




