from datetime import datetime
from rest_framework import serializers

from notificationService import models


class DistributionSerializer(serializers.ModelSerializer):

    def validate(self, attrs):
        try:
            if datetime.now().timestamp() > attrs.get('dt_end').timestamp():
                raise serializers.ValidationError({"dt_end": "Время конца должно быть больше текущего времени"})
            if attrs.get('dt_start') > attrs.get('dt_end'):
                raise serializers.ValidationError({"dt_start": "Время конца должно быть больше времени начала"})
            return attrs
        except AttributeError:
            if attrs.get('dt_start') is None:
                raise serializers.ValidationError({"dt_start": "Не передано время"})
            if attrs.get('dt_end') is None:
                raise serializers.ValidationError({"dt_end": "Не передано время"})

    class Meta:
        model = models.Distribution
        fields = "__all__"


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Message
        fields = "__all__"
