from django.dispatch import receiver

from notificationService.tasks import *

logger = logging.getLogger(__name__)


@receiver(models.signals.post_save, sender=Distribution, dispatch_uid="mailing_list")
def mailing_list(sender, instance, created, **kwargs):
    if created:
        distribution = Distribution.objects.filter(id=instance.id).first()
        logger.info(f"Начата рассылка id={distribution.id}")
        clients = Client.objects.filter(models.Q(opr_code=distribution.filter_code) |
                                        models.Q(tag=distribution.filter_tag)).all()
        for client in clients:
            message = Message.objects.create(
                client=client,
                distribution=instance,
            )
            logger.info(f"Создано сообщение id={message.id}")
            api_data = {
                'id': message.id,
                'phone': client.phone,
                'text': distribution.message
            }
            if distribution.sending:
                task = sending_message.apply_async((api_data, distribution.id, client.id),)
            else:
                task = sending_message.apply_async((api_data, distribution.id, client.id),
                                            eta=distribution.dt_start)

            logger.info(f"Создана задача id={task.id}, state={task.state}, status={task.status}")

