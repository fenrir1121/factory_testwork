import datetime
import logging
import requests
import pytz
from dateutil import tz

from django.conf import settings

from fvProject.celery import app
from celery import shared_task

from notificationService.models import *

logger = logging.getLogger(__name__)


@app.task(bind=True, retry_backoff=True)
def sending_message(self, message_data, dist_id, client_id):
    # получаем рассылку и клиента
    dist = Distribution.objects.get(id=dist_id)
    client = Client.objects.get(id=client_id)

    # получаем время клиента
    tz = pytz.timezone(client.utc)
    now = datetime.now(tz)

    # конвертируем datetime с учетом пояса клиента
    start = tz.localize(
        datetime(dist.dt_start.year, dist.dt_start.month, dist.dt_start.day, dist.dt_start.hour, dist.dt_start.minute))
    end = tz.localize(
        datetime(dist.dt_end.year, dist.dt_end.month, dist.dt_end.day, dist.dt_end.hour, dist.dt_end.minute,
                 dist.dt_end.second))

    if start <= now <= end:
        header = {
            'Authorization': f'Bearer {settings.EXT_API_TOKEN}',
            'Content-Type': 'application/json'}
        try:
            resp = requests.post(url=settings.EXT_API_URL + str(message_data['id']), headers=header, json=message_data)
        except requests.RequestException:
            pass
            logger.warning(f"Сообщение id={str(message_data['id'])} не отправлено")
        else:
            if resp.status_code == 200:
                logger.info(f"Сообщение id={message_data['id']} отправлено")
                Message.objects.filter(id=message_data['id']).update(sending=True, created_at=datetime.now(tz=None))
            else:
                logger.warning(f"Возвращен код {resp.status_code}, задача будет выполнена повторно")
                return self.retry(args=(message_data, dist_id, client_id), countdown=60*60)
    else:
        # если время отправки в будущем
        time_delta = int(start.timestamp()) - int(now.timestamp())
        if time_delta >= 0:
            logger.info(f"Сообщение id={message_data['id']} будет отправлено через {time_delta} сек")
            return self.retry(args=(message_data, dist_id, client_id), countdown=time_delta)
        else:
            logger.warning(f"По часовому поясу клиента '{client.utc}' время рассылки уже в прошлом "
                           f"и сообщение id={message_data['id']} не может быть доставлено")

