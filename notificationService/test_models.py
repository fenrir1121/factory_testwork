from rest_framework.test import APITestCase
from django.utils.timezone import now, timedelta

from .models import Distribution, Client, Message


class TestModel(APITestCase):

    def test_client(self) -> Client:
        client = Client.objects.create(
            phone='79991234567',
            opr_code='999',
            tag='test',
            utc='Europe/Moscow'
        )
        self.assertIsInstance(client, Client)
        return client

    def test_distribution(self) -> Distribution:
        distribution = Distribution.objects.create(
            dt_start=now(),
            message='test message',
            filter='test',
            dt_end=now() + timedelta(hours=1)
        )
        self.assertIsInstance(distribution, Distribution)
        return distribution

    def test_message(self) -> None:
        client = self.test_client()
        distribution = self.test_distribution()
        message = Message.objects.create(
            created_at=now(),
            sending=False,
            distribution=distribution,
            client=client
        )
        self.assertIsInstance(message, Message)
