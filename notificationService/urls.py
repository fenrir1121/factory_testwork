from rest_framework import routers

from notificationService.views import *

router = routers.DefaultRouter()
router.register(r'distributions', DistributionViewSet)
router.register(r'clients', ClientViewSet)
router.register(r'messages', MessageViewSet)

urlpatterns = router.urls
