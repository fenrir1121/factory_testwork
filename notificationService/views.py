from rest_framework import viewsets

from notificationService.tasks import *
from notificationService import serializers

logger = logging.getLogger(__name__)


class DistributionViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.DistributionSerializer
    queryset = Distribution.objects.all()


class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ClientSerializer
    queryset = Client.objects.all()


class MessageViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.MessageSerializer
    queryset = Message.objects.all()